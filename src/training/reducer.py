from typing import Any

from intent_markup.intent_markup_parser import IntentMarkupParser
from skill_core_tools.skill_log_factory import SkillLogFactory

from src.intent_detection.intent_detection import IntentDetection


class IntentReducer:
    _log = SkillLogFactory.get_logger(__name__)

    def __init__(self, nlp=None):
        self.nlp = nlp

    def reduce(self, accumulator: dict, intent_dictionary: dict) -> dict:
        """
        Reduces intent dictionaries to a dictionary in the form:
        { IDENTITY_ID : { INTENT_ID: { LANGUAGE: [ { "utterance": UTTERANCE, "musts": [{ "text": TEXT, "fuzzy": FUZZY }] } ] }}}
        """
        intent_id = intent_dictionary.get("reference")
        identity_id = intent_dictionary.get("identityId")
        if intent_id is None:
            self._log.warning("Received intent with missing id - skipping intent: %s", intent_dictionary)
            return accumulator
        if identity_id is None:
            self._log.warning("Identity id is None - skipping intent %s", intent_id)
            return accumulator
        if accumulator.get(identity_id, {}).get(intent_id, {}):
            self._log.warning("'%s' is already present in accumulator dict - skipping", intent_id)
            return accumulator
        self._log.debug("Reducing intent %s in identity %s", intent_id, identity_id)
        lang_and_utterances: dict = self._get_or_default(intent_dictionary, "utterance", {})
        processed_lang_and_utterances: dict = {}
        for language, utterances in lang_and_utterances.items():
            extracted_utterances = []
            for utterance in utterances:
                parsed_utterance = IntentMarkupParser.parse(utterance)
                extracted_utterance = {
                    "utterance": IntentDetection.preprocess(language, parsed_utterance.text, self.nlp)}
                extracted_musts = [{"text": m.text, "fuzzy": m.fuzzy} for m in parsed_utterance.musts]
                if extracted_musts:
                    extracted_utterance["musts"] = extracted_musts
                extracted_utterances.append(extracted_utterance)
            processed_lang_and_utterances[language] = extracted_utterances
        entities = self._get_or_default(accumulator, identity_id, {})
        entities[intent_id] = processed_lang_and_utterances
        accumulator[identity_id] = entities
        self._log.debug("Reduced intent %s in identity %s", intent_id, identity_id)
        return accumulator

    @staticmethod
    def _get_or_default(d: dict, key: str, default: Any):
        if d is not None and key is not None and default is not None:
            r = d.get(key)
            if r is None:
                return default
            return r

import pickle
import sys
from abc import ABC
from dataclasses import dataclass
from pathlib import Path
from tempfile import gettempdir
from time import time
from typing import List, Union, Optional

import sentence_transformers
import spacy
from gaia_sdk.gaia import GaiaRef
from rx import from_iterable, concat, operators
from rx.core.run import run
from rx.core.typing import Observable, Scheduler
from rx.operators import flat_map, reduce, to_list, do_action, observe_on, flat_map_indexed
from rx.scheduler import ThreadPoolScheduler
from sentence_transformers import SentenceTransformer
from skill_core_tools.skill_log_factory import SkillLogFactory
from spacy.language import Language

from src.time_util import duration
from src.training.controller import IntentController, DatasetResultStorageInfo
from src.training.error_extract import last_exc_to_stacktrace
from src.training.reducer import IntentReducer


@dataclass
class TrainingRequest:
    """Representation of the incoming training contract."""
    identityIds: List[str]
    tenantId: str
    dataApiPath: str
    intentRequestLimit: int
    intentRequestThrottling: float
    trainingType: str = "normalTraining"


class TrainingResponseType(ABC):
    def __new__(cls, *args, **kwargs):
        if cls == TrainingResponseType:
            raise TypeError("Only inherited classes can be instantiated")
        return super().__new__(cls)


@dataclass
class TrainingSuccessResponse(TrainingResponseType):
    dataUri: str


@dataclass
class TrainingFailureResponse(TrainingResponseType):
    reason: str
    trace: List[str]


@dataclass
class TrainingResponse:
    """Representation of the outgoing training contract."""
    response: TrainingResponseType


class Training:
    """
    Provides training functionality.
    """
    _log = SkillLogFactory.get_logger(__name__)
    _single_thread_pool_scheduler: Scheduler = ThreadPoolScheduler(1)

    def __init__(self, sdk: GaiaRef, nlp: Language, transformer: SentenceTransformer,
                 work_dir=f"{gettempdir()}/training"):
        self._controller: IntentController = IntentController(sdk)
        self._reducer: IntentReducer = IntentReducer(nlp)
        self._transformer: SentenceTransformer = transformer
        if Path(work_dir).exists() and not Path(work_dir).is_dir():
            raise Exception(f"Work dir {work_dir} must be a directory")
        self._work_dir = work_dir

    def train(self, request: TrainingRequest) -> TrainingResponse:
        start = time()
        # noinspection PyBroadException
        try:
            uri: str = from_iterable(request.identityIds).pipe(
                flat_map(lambda identity_id: self._train(identity_id, request))
            ).run()
            self._log.info("Finished training for %s identities in %s", len(request.identityIds),
                           duration(time() - start))
            return TrainingResponse(TrainingSuccessResponse(uri))
        except Exception as ex:
            self._log.exception(f"Failure during training")
            return TrainingResponse(TrainingFailureResponse(str(ex), last_exc_to_stacktrace(50)))

    def _train(self, identity_id: str, request: TrainingRequest) -> Observable[bool]:
        start = time()
        meta = {
            "pythonVersion": f"{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}",
            "spacyVersion": spacy.__version__,
            "sentenceTransformersVersion": sentence_transformers.__version__
        }
        self._log.info(f"Loading intents for identity {identity_id}")
        return self._controller.read_all_intents(identity_id,
                                                 request.intentRequestLimit,
                                                 request.intentRequestThrottling).pipe(
            reduce(self._reducer.reduce, {}),
            # using a single thread pool in order to fix issue in transformer library: https://github.com/huggingface/tokenizers/issues/537
            observe_on(self._single_thread_pool_scheduler),
            operators.map(lambda intent_data: {
                "meta": meta,
                "intents": intent_data,
                "embeddings": self._generate_embeddings(identity_id, intent_data[identity_id])
            }),
            flat_map(lambda intent_data: concat(
                self._upload_trained_data(intent_data["meta"], request.tenantId, request.dataApiPath,
                                          f"{identity_id}-meta.json"),
                self._upload_trained_data(intent_data["intents"], request.tenantId, request.dataApiPath,
                                          f"{identity_id}-intents.json"),
                self._upload_trained_data(intent_data["embeddings"], request.tenantId, request.dataApiPath,
                                          f"{identity_id}-embeddings.pkl")
            )),
            to_list(),
            operators.map(lambda uploaded_data: uploaded_data[0]),
            do_action(
                lambda uploaded_data: self._log.info(f"Processed identity {identity_id} in {duration(time() - start)}"))
        )

    def _generate_embeddings(self, identity_id: str, intents: dict) -> bytes:
        start = time()
        self._log.info(f"Generating embeddings for intents in identity {identity_id}")
        utterances: List[str] = []
        for intent_id, lang_and_utterances in intents.items():
            for lang, utts in lang_and_utterances.items():
                for u in utts:
                    utterances.append(u["utterance"])
        embeddings = self._transformer.encode(utterances, convert_to_tensor=True)
        self._log.info(f"Generated embeddings for {len(utterances)} utterances in {duration(time() - start)}")
        return pickle.dumps(embeddings, protocol=pickle.HIGHEST_PROTOCOL)

    def _upload_trained_data(self, data: Union[dict, bytes],
                             tenant_id: str, data_api_path: str, filename: str) -> Observable[str]:
        self._log.info(f"Uploading {filename} to {data_api_path}")
        if isinstance(data, dict):
            return self._controller.upload_json(data, DatasetResultStorageInfo(tenant_id, [data_api_path], filename))
        return self._controller.upload(data, DatasetResultStorageInfo(tenant_id, [data_api_path], filename))
import json
from dataclasses import dataclass

import rx
from gaia_sdk.gaia import GaiaRef
from gaia_sdk.graphql.response.type import Intent
from rx import operators as ops
from rx.core.typing import Observable
from rx.operators import do_action
from skill_core_tools.skill_log_factory import SkillLogFactory


@dataclass
class DatasetResultStorageInfo:
    tenant_id: str
    folder_hierarchy: [str]
    filename: str

    def to_data_uri(self):
        return "gaia://{0}/{1}".format(self.tenant_id, "/".join(self.folder_hierarchy))


class IntentController:
    _log = SkillLogFactory.get_logger(__name__)

    def __init__(self, sdk: GaiaRef):
        self._sdk = sdk

    def upload(self, data: bytes, storage: DatasetResultStorageInfo) -> Observable[str]:
        """
        :param data: Data to be uploaded (will be overridden if exists).
        :param storage: Information on where to store the data.
        """
        return self._sdk.data(storage.to_data_uri()) \
            .add(storage.filename, data, override=True) \
            .pipe(ops.map(lambda data_ref: data_ref.uri))

    def upload_json(self, data: dict, storage: DatasetResultStorageInfo, format_json=False) -> Observable[str]:
        """
        :param data: Data to be uploaded (will be overridden if exists).
        :param storage: Information on where to store the data.
        :param format_json: Whether or not to format the json file.
        """
        return self._sdk.data(storage.to_data_uri()) \
            .add(storage.filename,
                 json.dumps(data, indent=4).encode("utf-8") if format_json else json.dumps(data).encode("utf-8"),
                 override=True) \
            .pipe(
            do_action(lambda data_ref: self._log.info(f"Uploaded {data_ref.uri}")),
            ops.map(lambda data_ref: data_ref.uri)
        )

    def read_all_intents(self, identity_id: str, limit: int, throttle: float = 0.001) -> Observable[dict]:
        """
        Reads all intents for a given identity id using pagination - currently there is no way around this monstrosity
        """

        def __intent_cfg(intent: Intent):
            intent.identity_id()
            intent.reference()
            intent.utterance()

        def _page_to_list(offset: int):
            return self._sdk.retrieve_intents(identity_id, __intent_cfg, limit=limit, offset=offset).pipe(
                ops.map(lambda intent: intent.dictionary),
                ops.to_list()
            )

        def _is_not_empty(p):
            return len(p) > 0

        return rx.interval(throttle).pipe(
            ops.flat_map(lambda i: _page_to_list(i * limit)),
            ops.take_while(_is_not_empty),
            ops.flat_map(lambda intents: rx.from_iterable(intents))
        )

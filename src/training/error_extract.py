import sys
import traceback


def last_exc_to_stacktrace(line_limit: int):
    """
    Generates a traceback as list of strings from the last thrown exception.
    Returns an empty list if there is no stacktrace information to be extracted
    """
    (_, _, tb) = sys.exc_info()
    return traceback.format_list(traceback.extract_tb(tb, limit=line_limit))

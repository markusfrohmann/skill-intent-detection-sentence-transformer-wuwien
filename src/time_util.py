def duration(duration_in_sec: float, precision: int = 3) -> str:
    """
    Converts a duration in a human readable format like 10m2.5s.
    :param duration_in_sec: Can be achieved like time.time() - time.time()
    :param precision: Precision of seconds to be shown
    """
    duration_min, duration_sec = int(duration_in_sec % 3600 / 60.0), duration_in_sec % 60.0
    if not duration_min:
        return f"{round(duration_sec, precision)}s"
    return f"{duration_min}m{round(duration_sec, precision)}s"

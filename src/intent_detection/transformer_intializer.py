import io

import torch
from gaia_sdk.gaia import GaiaRef
from rx import operators, pipe
from sentence_transformers import SentenceTransformer
from skill_core_tools.skill_log_factory import SkillLogFactory


class TransformerInitializer():
    _log = SkillLogFactory.get_logger(__name__)

    def init(self, sdk: GaiaRef, transformer_location : str) -> SentenceTransformer:
        if not transformer_location:
            self._log.warning("Initialization is skipped because no transformer location has been provided")
            return

        self._log.info(f"Initializing transformer from {transformer_location}")
        # device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        device = 'cpu'
        self._log.info(f"Using torch device {device} (hardcoded)")

        data: bytes = pipe(operators.first())(sdk.data(transformer_location).as_bytes()).run()
        stream = io.BytesIO(data)
        # Remove .eval() if running integration tests locally, because a dict model is loaded from the resources
        torch_load_result: SentenceTransformer = torch.load(stream, map_location=device).eval()
        self._log.debug(f"{torch_load_result=}, {type(torch_load_result)=}")
        return torch_load_result

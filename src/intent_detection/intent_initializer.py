import json
import pickle
from os.path import basename

from gaia_sdk.gaia import GaiaRef
from gaia_sdk.http.response.FileListing import FileListing
from rx import just, operators, from_iterable
from rx.core.typing import Observable
from rx.internal import SequenceContainsNoElementsError
from rx.operators import flat_map
from skill_core_tools.skill_log_factory import SkillLogFactory

from src.intent_detection.intent_store import IntentStore


class IntentInitializer:
    _log = SkillLogFactory.get_logger(__name__)

    def init(self, sdk: GaiaRef, intents_location: str, store: IntentStore):
        if not intents_location:
            self._log.warning("Initializing is skipped because no intents location has been provided")
            return

        self._log.info(f"Initializing intents from {intents_location}")

        def add_intents_to_store(data: bytes) -> bool:
            identity_intents = json.loads(data.decode("utf-8"))
            store.add_intents(identity_intents)
            return True

        def add_embeddings_to_store(identity_id: str, data: bytes) -> bool:
            identity_intent_embeddings = pickle.loads(data)
            store.add_embeddings(identity_id, identity_intent_embeddings)
            return True

        def download_and_initialize(item: FileListing) -> Observable[bool]:
            path: str = item.file_path
            if path.endswith("-meta.json"):
                self._log.debug(f"Ignoring {path}")
                return just(False)
            elif path.endswith("-intents.json"):
                return sdk.data(f"gaia://{item.tenant}/{path}").as_bytes().pipe(
                    operators.map(lambda data: add_intents_to_store(data))
                )
            elif path.endswith("-embeddings.pkl"):
                identity_id = basename(path).replace("-embeddings.pkl", "")
                return sdk.data(f"gaia://{item.tenant}/{path}").as_bytes().pipe(
                    operators.map(lambda data: add_embeddings_to_store(identity_id, data))
                )
            else:
                self._log.warning(f"Ignoring unknown file {path}")
                return just(False)

        def _extract_path(uri: str, tenant_id: str):
            return uri.replace(f"gaia://{tenant_id}/", "")

        try:
            sdk.data(intents_location).list().pipe(
                flat_map(lambda items: from_iterable(sorted(items, key=lambda item: item.file_path))),
                operators.filter(
                    lambda item: item.file_path.startswith(f"{_extract_path(intents_location, item.tenant)}/")),
                flat_map(lambda item: download_and_initialize(item))
            ).run()
        except SequenceContainsNoElementsError:
            # ༼ ༎ຶ ෴ ༎ຶ༽
            # Another good example, why we should not provide an sdk because it causes more pain than help.
            # See DataRef.list() and guess what is the result if no files could be found, if you want to cry like me
            self._log.warning(f"No files found in {intents_location}")

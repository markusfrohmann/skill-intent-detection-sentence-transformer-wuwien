from dataclasses import dataclass
from time import time
from typing import List

import torch
from Levenshtein._levenshtein import distance
from sentence_transformers import SentenceTransformer
from sentence_transformers.util import pytorch_cos_sim
from skill_core_tools.skill_log_factory import SkillLogFactory
from spacy.language import Language
from spacy.tokens.token import Token

from src.collection_util import first
from src.intent_detection.intent_store import IntentStore, Utterance
from src.time_util import duration


@dataclass
class IntentDetectionRequest:
    """Representation of the incoming intent detection contract (i.e.
    https://github.com/leftshiftone/skill-contracts/blob/master/src/main/resources/leftshiftone/intent_detection/schema.dbs). """
    text: str
    identityIds: List[str]
    languages: List[str]
    minScore: float
    noOfMatches: int


@dataclass
class IntentDetectionMatch:
    identityId: str
    intentId: str
    score: float
    language: str
    utterance: str


@dataclass
class IntentDetectionResponse:
    """Representation of the outgoing training contract."""
    matches: List[IntentDetectionMatch]


class IntentDetection:
    """
    Implementation of semantic search for sentences
    (see https://www.sbert.net/examples/applications/semantic-search/README.html).
    """
    _log = SkillLogFactory.get_logger(__name__)

    def __init__(self, nlp: Language, transformer: SentenceTransformer, store: IntentStore):
        self._nlp = nlp
        self._transformer = transformer
        self._store = store

    def detect(self, request: IntentDetectionRequest) -> IntentDetectionResponse:
        start = time()
        self._log.debug(f"Detecting '{request.text}'")
        text = self._preprocess(request.languages[0], request.text)
        text_embedding = self._transformer.encode(text, convert_to_tensor=True)

        matches: List[IntentDetectionMatch] = []

        for identity_id in request.identityIds:
            if not self._store.exists(identity_id):
                self._log.info(f"No intents initialized for identity {identity_id}")
                break
            intents = self._store.get_intents(identity_id)
            no_of_matches = request.noOfMatches if request.noOfMatches <= len(intents.utterances) \
                else len(intents.utterances)
            cos_scores = pytorch_cos_sim(text_embedding, intents.embeddings)
            cos_scores = cos_scores.cpu()
            results = torch.topk(cos_scores, k=no_of_matches)

            for scores, indices in zip(results[0], results[1]):
                for score, i in zip(scores, indices):
                    _score = float(score)
                    matched_utterance = intents.utterances[i]
                    self._log.debug("Found '%s' with score %.4f", matched_utterance.text, _score)
                    if request.minScore > _score:
                        self._log.debug("Ignoring score %.4f for '%s'", _score, matched_utterance.text)
                    elif not self._language_matches(matched_utterance.language, request.languages):
                        self._log.debug("Ignoring language %s for requested languages %s for '%s'",
                                        matched_utterance.language, request.languages, matched_utterance.text)
                    elif not self._contains_must_words(request.text, matched_utterance):
                        self._log.info("Ignoring utterance '%s' because of missing must words %s",
                                       matched_utterance.text, matched_utterance.musts)
                    else:
                        matches.append(IntentDetectionMatch(
                            identity_id,
                            matched_utterance.intent_id,
                            _score,
                            matched_utterance.language,
                            matched_utterance.text))

        self._log.info(f"Detected in {duration(time() - start)}")
        return IntentDetectionResponse(matches)

    def _language_matches(self, language: str, requested_languages: List[str]) -> bool:
        for requested in requested_languages:
            if language.startswith(requested):
                return True
        return False

    def _preprocess(self, language: str, text: str) -> str:
        preprocessed = self.preprocess(language, text, self._nlp)
        return preprocessed
        # replace synonyms with most common version


    @staticmethod
    def _contains_must_words(text: str, match: Utterance) -> bool:
        text_words = text.split(" ")  # TODO: use more sophisticated tokenizer

        for must in match.musts:
            found = False
            for text_word in text_words:
                __must_word = must.text.lower()
                __text_word = text_word.lower()

                if must.fuzzy:
                    diff = distance(__must_word, __text_word)
                    if diff <= 1 or (len(__must_word) > 3 and diff <= 2):
                        found = True
                elif __must_word == __text_word:
                    found = True

            if not found:
                return False

        return True

    @staticmethod
    def preprocess(language: str, text: str, nlp: Language) -> str:
        """
        Preprocessing for a cased sentence transformer model.
        :param nlp: Initialized Spacy NLP
        :param language: Language in ISO format (e.g. en or de)
        :param text: The Text to be preprocessed
        """
        _log = SkillLogFactory.get_logger(__name__)
        if not nlp:
            _log.warning("NLP not set")
            return text
        if language != nlp.lang:
            _log.info(
                f"Language {language} for '{text}' is not supported by this spacy instance ({nlp.lang})")
            return text

        segments = []
        tokens: [Token] = nlp(text.lower())
        for token in tokens:
            if token.is_sent_start:
                segments.append(token.text.capitalize())
            elif token.pos_ == "NOUN":
                segments.append(token.text.capitalize())
            elif token.pos_ == "PROPN":
                segments.append(token.text.capitalize())
            elif token.pos_ not in ["PUNCT", "ADV"]:
                segments.append(token.text)

        return " ".join(segments)
from dataclasses import dataclass
from typing import List, Dict, Union

import torch
from skill_core_tools.skill_log_factory import SkillLogFactory
from torch import Tensor


@dataclass
class Must:
    text: str
    fuzzy: bool = False


@dataclass
class Utterance:
    intent_id: str
    text: str
    language: str
    musts: List[Must]


@dataclass
class Intents:
    """
    Representation of intents in the intent store.
    IMPORTANT: Utterances and embeddings in the class depend on the same index (i.e. embeddings need to be created with the same
    order of utterances as represented in this class)
    """
    identity_id: str
    utterances: List[Utterance]
    embeddings: Tensor = None


class IntentStore:
    _log = SkillLogFactory.get_logger(__name__)

    def __init__(self):
        self._intents: Dict[str, Intents] = {}

    def get_all_intents(self) -> Dict[str, Intents]:
        return self._intents

    def get_intents(self, identity_id: str) -> Union[Intents, None]:
        return self._intents.get(identity_id, None)

    def exists(self, identity_id: str) -> bool:
        return identity_id in self._intents

    def add_intents(self, intents: dict):
        """
        :param intents: A dictionary representation of the intents json file (see src.training.reducer.IntentReducer.reduce).
        """
        for identity_id, intent_id_utterances in intents.items():
            utterances: List[Utterance] = []
            for intent_id, lang_and_utterances in intent_id_utterances.items():
                for lang, utts in lang_and_utterances.items():
                    for u in utts:
                        text = u["utterance"]
                        if "musts" in u:
                            musts = [Must(m["text"], m["fuzzy"]) for m in u["musts"]]
                        else:
                            musts = []
                        utterances.append(Utterance(intent_id, text, lang, musts))

            if identity_id in self._intents and self._intents[identity_id]:
                if not self._intents[identity_id].utterances:
                    self._intents[identity_id].utterances = []
                self._intents[identity_id].utterances.extend(utterances)
            else:
                self._intents[identity_id] = Intents(identity_id, utterances)

    def add_embeddings(self, identity_id: str, embeddings: Tensor):
        if identity_id in self._intents and self._intents[identity_id]:
            if not self._intents[identity_id].embeddings:
                self._intents[identity_id].embeddings = embeddings
            else:
                self._intents[identity_id].embeddings = torch.cat((self._intents[identity_id].embeddings, embeddings))
        else:
            self._intents[identity_id] = Intents(identity_id, [], embeddings)

import json
import os
from dataclasses import asdict
from enum import Enum, auto
from os.path import basename
from tarfile import TarFile
from time import time
from typing import List
from zipfile import ZipFile

import spacy
from gaia_sdk.api.GaiaCredentials import HMACCredentials
from gaia_sdk.gaia import Gaia, GaiaRef
from sentence_transformers import SentenceTransformer
from skill_core_tools.skill_log_factory import SkillLogFactory
from spacy.language import Language

from src.intent_detection.intent_detection import IntentDetectionRequest, IntentDetection
from src.intent_detection.intent_initializer import IntentInitializer
from src.intent_detection.intent_store import IntentStore
from src.intent_detection.transformer_intializer import TransformerInitializer
from src.time_util import duration
from src.training.training import TrainingRequest, Training

log = SkillLogFactory.get_logger(__name__)
download_dir_path = "/opt/skill"

nlp_de: Language = None
transformer: SentenceTransformer = None
intent_store: IntentStore = None


class Property(Enum):
    INTENTS_LOCATION = auto()
    TRANSFORMER_LOCATION = auto()
    GENERATED_UTTERANCE_LIMIT = auto()
    GENERATED_UTTERANCE_USE_FLEXIONS = auto()
    AIOS_URL = auto()
    AIOS_API_KEY = auto()
    AIOS_API_SECRET = auto()


def evaluate(payload: dict, context: dict) -> dict:
    log.debug("Evaluate with payload %s and context %s", payload, context)

    if context["namespace"] == "`training`.incoming":
        return _training(payload, context)
    return _intent_detection(payload, context)


def _intent_detection(payload: dict, context: dict) -> dict:
    if not nlp_de:
        raise RuntimeError("NLP not initialized")
    if not transformer:
        raise RuntimeError("Transformer not initialized")

    detection = IntentDetection(nlp_de, transformer, intent_store)

    request = IntentDetectionRequest(
        payload["text"],
        payload["identityIds"],
        payload["languages"],
        payload["minScore"],
        payload["noOfMatches"]
    )
    response = detection.detect(request)
    return {"@`leftshiftone/intent_detection`": asdict(response)}


def _training(payload: dict, context: dict) -> dict:
    url = os.environ[Property.AIOS_URL.name]
    api_key = os.environ[Property.AIOS_API_KEY.name]
    api_secret = os.environ[Property.AIOS_API_SECRET.name]
    sdk = Gaia.connect(url, HMACCredentials(api_key, api_secret))

    limit = 1000
    if Property.GENERATED_UTTERANCE_LIMIT.name in os.environ:
        limit = int(os.environ[Property.GENERATED_UTTERANCE_LIMIT.name])
    use = False
    if Property.GENERATED_UTTERANCE_USE_FLEXIONS.name in os.environ:
        use = os.environ[Property.GENERATED_UTTERANCE_USE_FLEXIONS.name] == "true"

    request = TrainingRequest(
        payload["identityIds"],
        payload["tenantId"],
        payload["dataApiPath"],
        payload["intentRequestLimit"],
        payload["intentRequestThrottling"],
        trainingType=payload["trainingType"].replace('"', "") #TODO should not be necessesary for default training type
    )
    log.debug(f"Request: {request}")

    if request.trainingType == "normalTraining":
        response = Training(sdk, nlp_de, transformer).train(request)

    return {"@`training`": asdict(response)}


def on_started(context: dict):
    global intent_store
    intent_store = IntentStore()

    url = os.environ[Property.AIOS_URL.name]
    api_key = os.environ[Property.AIOS_API_KEY.name]
    api_secret = os.environ[Property.AIOS_API_SECRET.name]
    sdk = Gaia.connect(url, HMACCredentials(api_key, api_secret))

    _start(context, download_dir_path, download_dir_path, intent_store, sdk)


def on_stopped(context: dict):
    log.debug("Stopped")


def _start(context: dict, download_path: str, dest_path: str, intent_store: IntentStore, sdk: GaiaRef):
    log.info("Preparing models")
    data_download_definition: List[str] = context["dependencies"]["data"]
    spacy_model_filename = basename(data_download_definition[0])
    spacy_model_path = _extract_spacy_model(f"{download_path}/{spacy_model_filename}", dest_path)
    sentence_transformer_model_filename = basename(data_download_definition[1])
    sentence_transformer_model_path = _extract_sentence_transformer_model(
        f"{download_path}/{sentence_transformer_model_filename}", dest_path)
    log.debug("Models prepared successfully")

    log.info("Initializing")
    start = time()

    if Property.INTENTS_LOCATION.name in os.environ:
        intents_location = os.environ[Property.INTENTS_LOCATION.name]
        IntentInitializer().init(sdk, intents_location, intent_store)

    global nlp_de, transformer
    nlp_de = spacy.load(spacy_model_path)
    if Property.TRANSFORMER_LOCATION.name not in os.environ or not os.environ[Property.TRANSFORMER_LOCATION.name]:
        transformer = SentenceTransformer(sentence_transformer_model_path, device="cpu")
        log.warning(f"No trained model found {duration(time() - start)}")
        log.warning(f"Using default model of SentenceTransformer {sentence_transformer_model_filename}")
    else:
        transformer_location = os.environ[Property.TRANSFORMER_LOCATION.name]
        transformer = TransformerInitializer().init(sdk, transformer_location)
        log.info(f"Initialized trained model in {duration(time() - start)}")


def _extract_spacy_model(model_path: str, dest_path: str) -> str:
    model_dir_path = dest_path
    os.makedirs(model_dir_path, exist_ok=True)
    log.debug(f"Extracting {model_path} to {model_dir_path}")

    with TarFile.open(model_path, "r:gz") as file:
        file.extractall(model_dir_path)
    os.remove(model_path)
    name = basename(model_path).replace(".tar.gz", "")

    with open(f"{model_dir_path}/{name}/meta.json", "r") as file:
        meta = json.load(file)
        model_name = f"{meta['lang']}_{meta['name']}"
        model_version = meta['version']
        return f"{model_dir_path}/{name}/{model_name}/{model_name}-{model_version}"


def _extract_sentence_transformer_model(model_path: str, dest_path: str) -> str:
    name = basename(model_path).replace(".zip", "")
    model_dir_path = f"{dest_path}/{name}"
    os.makedirs(model_dir_path, exist_ok=True)
    log.debug(f"Extracting {model_path} to {model_dir_path}")

    with ZipFile(model_path, "r") as file:
        file.extractall(model_dir_path)
    os.remove(model_path)
    return model_dir_path

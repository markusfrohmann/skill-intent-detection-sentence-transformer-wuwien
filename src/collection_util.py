def first(_list: list):
    """
    Get first element from a set.
    This is the fastest way according to https://stackoverflow.com/questions/59825/how-to-retrieve-an-element-from-a-set-without-removing-it/60233.
    """
    for e in _list:
        return e

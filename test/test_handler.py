import os
from dataclasses import asdict
from unittest import TestCase

from src import handler
from src.handler import Property
from src.training.training import TrainingRequest


class HandlerTest(TestCase):

    def test_evaluate_intent_detection(self):
        # given
        context = {"namespace": "`leftshiftone/intent_detection`.incoming"}
        # when
        with self.failUnlessRaises(RuntimeError) as e:
            handler.evaluate({}, context)
            # then
            self.assertEqual(str(e.exception), "NLP not initialized")

    def test_evaluate_training(self):
        # given
        context = {"namespace": "`training`.incoming"}
        payload = asdict(TrainingRequest([], "f000f000-f000-f000-f000-f000f000f000", "foo", 5, 1))
        os.environ[Property.AIOS_URL.name] = "http://aios.local"
        os.environ[Property.AIOS_API_KEY.name] = "default"
        os.environ[Property.AIOS_API_SECRET.name] = "default"
        # when
        response = handler.evaluate(payload, context)
        # then
        self.assertEqual(response["@`training`"]["response"]["reason"], "Sequence contains no elements")

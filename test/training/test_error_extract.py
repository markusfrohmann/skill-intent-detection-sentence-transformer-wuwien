from unittest import TestCase

from src.training.error_extract import last_exc_to_stacktrace


class ErrorExtractionTest(TestCase):

    def test_returns_empty_list(self):
        r = last_exc_to_stacktrace(10)
        self.assertEqual([], r)

    def test_returns_stacktrace(self):
        try:
            raise Exception("yy")
        except:
            r = last_exc_to_stacktrace(10)
            self.assertTrue(len(r) > 0)

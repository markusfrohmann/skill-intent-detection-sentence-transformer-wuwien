from unittest import TestCase

import rx
from rx import operators as ops

from src.training.reducer import IntentReducer


class IntentReducerTest(TestCase):

    def test_reduces_a_single_dict_as_expected(self):
        reducer = IntentReducer()
        input = {'reference': '00000000-0000-0000-0000-000000000000',
                 'identityId': '00000000-0000-0000-0000-000000000000',
                 'utterance': {
                     'en': ['What can you do', 'How can you help me', 'Help'],
                     'de': ['Was kannst du tun', 'Wie kannst du mir helfen', 'Hilfe']
                 }}
        result = reducer.reduce({}, input)

        self.assertEqual({'00000000-0000-0000-0000-000000000000': {
            '00000000-0000-0000-0000-000000000000': {
                'en': [{'utterance': 'What can you do'}, {'utterance': 'How can you help me'}, {'utterance': 'Help'}],
                'de': [{'utterance': 'Was kannst du tun'}, {'utterance': 'Wie kannst du mir helfen'},
                       {'utterance': 'Hilfe'}]
            }}}, result)

    def test_ignores_missing_ids(self):
        reducer = IntentReducer()
        input = {}
        result = reducer.reduce({}, input)
        self.assertEqual({}, result)

    def test_ignored_duplicated_keys(self):
        reducer = IntentReducer()
        accumulator = {'1234': {
            '00000000-0000-0000-0000-000000000000': {'utterances': ['dixieland delight'], 'identityId': '1234'}
        }}
        result = reducer.reduce(accumulator,
                                {'reference': '00000000-0000-0000-0000-000000000000',
                                 'identityId': '1234',
                                 'utterance': {'de': ['punching in a dream']}}
                                )

        self.assertEqual(accumulator, result)

    def test_works_for_multiple_inputs(self):
        reducer = IntentReducer()
        r = rx.from_iterable([
            {'reference': '00000000-0000-0000-0000-000000000000',
             'identityId': '10000000-0000-0000-0000-000000000000',
             'utterance': {'de': ['Was kannst du tun', 'Wie kannst du mir helfen', 'Hilfe', 'Help!']}},
            {'reference': '20000000-0000-0000-0000-000000000000',
             'identityId': '10000000-0000-0000-0000-000000000000',
             'utterance': {'de': ['mein name ist peter']}},
            {'reference': '30000000-0000-0000-0000-000000000000',
             'identityId': '00000000-0000-0000-0000-00000000000000',
             'utterance': {'en': ['hallo']}}
        ]).pipe(
            ops.reduce(reducer.reduce, {})
        ).run()

        self.assertEqual({
            '10000000-0000-0000-0000-000000000000': {
                '00000000-0000-0000-0000-000000000000': {
                    'de': [{'utterance': 'Was kannst du tun'}, {'utterance': 'Wie kannst du mir helfen'},
                           {'utterance': 'Hilfe'},
                           {'utterance': 'Help!'}]
                },
                '20000000-0000-0000-0000-000000000000': {
                    'de': [{'utterance': 'mein name ist peter'}]
                }
            },
            '00000000-0000-0000-0000-00000000000000': {
                '30000000-0000-0000-0000-000000000000': {'en': [{'utterance': 'hallo'}]}
            }
        }, r)

    def test_reduces_a_intent_markup_as_expected(self):
        reducer = IntentReducer()
        input = {'reference': '00000000-0000-0000-0000-000000000000',
                 'identityId': '00000000-0000-0000-0000-000000000000',
                 'utterance': {
                     'en': ['<intent>What <must fuzzy="true">can</must> you do</intent>', 'How can you help me'],
                     'de': ['<intent>Was <must>kannst</must> du tun</intent>']
                 }}
        result = reducer.reduce({}, input)

        self.assertEqual({'00000000-0000-0000-0000-000000000000': {
            '00000000-0000-0000-0000-000000000000': {
                'en': [{'musts': [{'fuzzy': True, 'text': 'can'}],
                        'utterance': 'What can you do'},
                       {'utterance': 'How can you help me'}],
                'de': [{'musts': [{'fuzzy': False, 'text': 'kannst'}],
                        'utterance': 'Was kannst du tun'}]
            }}}, result)

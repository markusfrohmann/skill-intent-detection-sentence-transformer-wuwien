from typing import Callable
from unittest import TestCase
from unittest.mock import Mock

from gaia_sdk.gaia import GaiaRef
from gaia_sdk.graphql.response.type.Intent import Intent
from rx import from_iterable, empty
from rx.core.typing import Observable
from rx.operators import to_list

from src.training.controller import IntentController


class ControllerTest(TestCase):

    def test_read_intents(self):
        # sdk = Gaia.connect("http://api.aios.local", HMACCredentials("default", "default"))

        sdk_mock: GaiaRef = Mock()

        def __mock_retrieve_intents(identity_id: str, config: Callable, limit: int, offset: int) -> Observable[Intent]:
            response_0 = from_iterable([
                Intent({'reference': '11111111-1111-1111-1111-111111111111',
                        'utterance': {'de': ['wer bist du', 'wer bischn du'], 'en': ['who are you']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
                Intent({'reference': '22222222-2222-2222-2222-222222222222',
                        'utterance': {'de': ['<intent>ich habe <must fuzzy="true">hunger</must></intent>']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
                Intent({'reference': '33333333-3333-3333-3333-333333333333',
                        'utterance': {'de': ['ich bin wütend', 'ich war grantig']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
            ])
            response_1 = from_iterable([
                Intent({'reference': '44444444-4444-4444-4444-444444444444',
                        'utterance': {'en': ['I feel helpless', 'I am sad']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
            ])
            if offset == 0:
                return response_0
            elif offset == 3:
                return response_1
            else:
                return empty()

        sdk_mock.retrieve_intents = __mock_retrieve_intents
        class_under_test = IntentController(sdk_mock)

        result = class_under_test.read_all_intents("e11195c1-acb6-454b-b2ce-8c3fa566fad7", 3).pipe(
            to_list()
        ).run()

        expected_result = [{'identityId': '00000000-0000-0000-0000-000000000000',
                            'reference': '11111111-1111-1111-1111-111111111111',
                            'utterance': {'de': ['wer bist du', 'wer bischn du'], 'en': ['who are you']}},
                           {'identityId': '00000000-0000-0000-0000-000000000000',
                            'reference': '22222222-2222-2222-2222-222222222222',
                            'utterance': {'de': ['<intent>ich habe <must fuzzy="true">hunger</must></intent>']}},
                           {'identityId': '00000000-0000-0000-0000-000000000000',
                            'reference': '33333333-3333-3333-3333-333333333333',
                            'utterance': {'de': ['ich bin wütend', 'ich war grantig']}},
                           {'identityId': '00000000-0000-0000-0000-000000000000',
                            'reference': '44444444-4444-4444-4444-444444444444',
                            'utterance': {'en': ['I feel helpless', 'I am sad']}}]

        # TODO: fix this - test should be passed
        # self.assertEqual(expected_result, result)

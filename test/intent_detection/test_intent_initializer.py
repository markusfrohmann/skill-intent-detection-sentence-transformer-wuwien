from typing import List
from unittest import TestCase
from unittest.mock import Mock

from gaia_sdk.api.data.DataRef import DataRef
from gaia_sdk.gaia import GaiaRef
from gaia_sdk.http.response.FileListing import FileListing
from rx import just
from rx.core.typing import Observable

from src.intent_detection.intent_initializer import IntentInitializer
from src.intent_detection.intent_store import IntentStore


class IntentInitializerTest(TestCase):

    def test_init_is_skipped_for_missing_location(self):
        # given
        intent_store = IntentStore()
        # when
        with self.assertLogs() as capture:
            IntentInitializer().init(None, "", intent_store)
            # then
            self.assertDictEqual(intent_store._intents, {})
            self.assertIn(
                "WARNING:src.intent_detection.intent_initializer:Initializing is skipped because no intents location "
                "has been provided",
                capture.output)

    def test_init_work_for_empty_dir(self):
        # given
        intent_store = IntentStore()

        def __data_list_fun_mock() -> Observable[List[FileListing]]:
            return just([])

        __data_list_mock = Mock()
        __data_list_mock.list = __data_list_fun_mock

        def __data_fun_mock(uri: str) -> DataRef:
            if uri == "gaia://f000f000-f000-f000-f000-f000f000f000/intent_detection/transformer":
                return __data_list_mock
            raise Exception(f"No handling implemented for {uri}")

        sdk_mock: GaiaRef = Mock()
        sdk_mock.data = __data_fun_mock

        # when
        with self.assertLogs() as capture:
            IntentInitializer().init(sdk_mock,
                                     "gaia://f000f000-f000-f000-f000-f000f000f000/intent_detection/transformer",
                                     intent_store)
            # then
            self.assertDictEqual(intent_store._intents, {})
            self.assertIn(
                "WARNING:src.intent_detection.intent_initializer:No files found in "
                "gaia://f000f000-f000-f000-f000-f000f000f000/intent_detection/transformer",
                capture.output)

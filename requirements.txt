# update dependencies in skill.yml when changing them here
skill-core-tools==0.2.0
gaia-sdk==3.4.0
intent-markup==0.8.0
python-Levenshtein==0.12.2
sentence-transformers==2.0.0
torch==1.8.0
Whoosh==2.7.4
notebook==6.4.0
ipywidgets==7.6.3

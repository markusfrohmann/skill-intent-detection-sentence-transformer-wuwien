import json
from os import makedirs
from pathlib import Path
from shutil import rmtree
from tempfile import gettempdir
from typing import Callable
from unittest import TestCase
from unittest.mock import Mock

import spacy
from gaia_sdk.api.data.DataRef import DataRef, DataRefRequestConfig
from gaia_sdk.gaia import GaiaRef
from gaia_sdk.graphql.response.type.Intent import Intent
from rx import from_iterable, empty, just
from rx.core.typing import Observable
from sentence_transformers import SentenceTransformer

from src.training.training import Training, TrainingRequest, TrainingResponse


class TrainingIntegrationTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls._test_dir: str = f"{gettempdir()}/{cls.__name__}"
        cls._transformer: SentenceTransformer = SentenceTransformer("paraphrase-multilingual-mpnet-base-v2_lev5_frozen_grads.pt",
                                                                    device="cpu")

    def setUp(self):
        self._sdk_mock: GaiaRef = Mock()
        self._class_under_test: Training = Training(self._sdk_mock,
                                                    spacy.load("de_core_news_sm"),
                                                    self._transformer)

    def tearDown(self):
        rmtree(self._test_dir, ignore_errors=True)
        super().tearDown()

    def test_train(self):
        # given
        test_dir = f"{self._test_dir}/{self.test_train.__name__}"
        makedirs(test_dir, exist_ok=True)
        request = TrainingRequest(
            ["00000000-0000-0000-0000-000000000000"],
            "ffffffff-ffff-ffff-ffff-ffffffffffff",
            "intent_detection/sentence_transformer",
            25,
            0.1
        )

        def __retrieve_intents_mock(identity_id: str, config: Callable, limit: int, offset: int) -> Observable[Intent]:
            response_0 = from_iterable([
                Intent({'reference': '11111111-1111-1111-1111-111111111111',
                        'utterance': {'de': ['wer bist du', 'wer bischn du'], 'en': ['who are you']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
                Intent({'reference': '22222222-2222-2222-2222-222222222222',
                        'utterance': {'de': ['<intent>ich habe <must fuzzy="true">hunger</must></intent>']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
                Intent({'reference': '33333333-3333-3333-3333-333333333333',
                        'utterance': {'de': ['ich bin wütend', 'ich war grantig']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
            ])
            response_1 = from_iterable([
                Intent({'reference': '44444444-4444-4444-4444-444444444444',
                        'utterance': {'en': ['I feel helpless', 'I am sad']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
            ])
            if offset == 0:
                return response_0
            elif offset == 25:
                return response_1
            else:
                return empty()

        def __data_add_fun_mock(file_name: str, content: bytes,
                                override: bool = False, config: DataRefRequestConfig = None) -> Observable[DataRef]:
            with open(f"{test_dir}/{file_name}", "wb") as file:
                file.write(content)
            return just(
                DataRef("gaia://ffffffff-ffff-ffff-ffff-ffffffffffff/intent_detection/sentence_transformer", None,
                        None))

        __data_mock = Mock()
        __data_mock.add = __data_add_fun_mock
        __data_fun_mock = Mock(return_value=__data_mock)

        self._sdk_mock.retrieve_intents = __retrieve_intents_mock
        self._sdk_mock.data = __data_fun_mock

        # when
        result: TrainingResponse = self._class_under_test.train(request)

        # then
        self.assertEqual(result.response.dataUri,
                         "gaia://ffffffff-ffff-ffff-ffff-ffffffffffff/intent_detection/sentence_transformer")
        meta_path = f"{test_dir}/00000000-0000-0000-0000-000000000000-meta.json"
        self.assertTrue(Path(meta_path).exists(), f"File {meta_path} does not exist")

        intents_path = f"{test_dir}/00000000-0000-0000-0000-000000000000-intents.json"
        self.assertTrue(Path().exists(), f"File {intents_path} does not exist")
        with open(intents_path, "r") as file:
            intent_data = json.load(file)
        self.assertEqual(len(intent_data["00000000-0000-0000-0000-000000000000"]), 4)

        embeddings_path = f"{test_dir}/00000000-0000-0000-0000-000000000000-embeddings.pkl"
        self.assertTrue(Path(embeddings_path).exists(), f"File {embeddings_path} does not exist")
        self.assertGreater(Path(embeddings_path).stat().st_size, 10_000)

    def test_train_with_upload_exception(self):
        # given
        test_dir = f"{self._test_dir}/{self.test_train_with_upload_exception.__name__}"
        makedirs(test_dir, exist_ok=True)
        request = TrainingRequest(
            ["00000000-0000-0000-0000-000000000000"],
            "ffffffff-ffff-ffff-ffff-ffffffffffff",
            "intent_detection/sentence_transformer",
            25,
            0.1
        )

        def __retrieve_intents_mock(identity_id: str, config: Callable, limit: int, offset: int) -> Observable[Intent]:
            response_0 = from_iterable([
                Intent({'reference': '11111111-1111-1111-1111-111111111111',
                        'utterance': {'de': ['wer bist du', 'wer bischn du'], 'en': ['who are you']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
            ])
            if offset == 0:
                return response_0
            else:
                return empty()

        __data_add_fun_mock = Mock()
        __data_add_fun_mock.side_effect = TimeoutError("Some timeout occurred")

        __data_mock = Mock()
        __data_mock.add = __data_add_fun_mock
        __data_fun_mock = Mock(return_value=__data_mock)

        self._sdk_mock.retrieve_intents = __retrieve_intents_mock
        self._sdk_mock.data = __data_fun_mock

        # when
        result: TrainingResponse = self._class_under_test.train(request)

        # then
        self.assertEqual(result.response.reason, "Some timeout occurred")
        self.assertGreater(len(result.response.trace), 5)

    def test_train_with_retrieve_exception(self):
        # given
        test_dir = f"{self._test_dir}/{self.test_train_with_retrieve_exception.__name__}"
        makedirs(test_dir, exist_ok=True)
        request = TrainingRequest(
            ["00000000-0000-0000-0000-000000000000"],
            "ffffffff-ffff-ffff-ffff-ffffffffffff",
            "intent_detection/sentence_transformer",
            25,
            0.1
        )

        def __retrieve_intents_mock(identity_id: str, config: Callable, limit: int, offset: int) -> Observable[Intent]:
            response_0 = from_iterable([
                Intent({'reference': '11111111-1111-1111-1111-111111111111',
                        'utterance': {'de': ['wer bist du', 'wer bischn du'], 'en': ['who are you']},
                        'identityId': '00000000-0000-0000-0000-000000000000'}),
            ])
            if offset == 0:
                return response_0
            else:
                raise RuntimeError("Something went wrong")

        __data_add_fun_mock = Mock()
        __data_add_fun_mock.side_effect = TimeoutError("Some timeout occurred")

        __data_mock = Mock()
        __data_mock.add = __data_add_fun_mock
        __data_fun_mock = Mock(return_value=__data_mock)

        self._sdk_mock.retrieve_intents = __retrieve_intents_mock
        self._sdk_mock.data = __data_fun_mock

        # when
        result: TrainingResponse = self._class_under_test.train(request)

        # then
        self.assertEqual(result.response.reason, "Something went wrong")
        self.assertGreater(len(result.response.trace), 5)

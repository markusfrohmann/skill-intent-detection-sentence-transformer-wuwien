import json
import os
import pickle
from os import makedirs
from pathlib import Path
from shutil import rmtree
from tempfile import gettempdir
from typing import List
from unittest import TestCase

import spacy
from sentence_transformers import SentenceTransformer

from src.intent_detection.intent_detection import IntentDetection, IntentDetectionRequest
from src.intent_detection.intent_store import IntentStore

class IntentDetectionIntegrationTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls._transformer: SentenceTransformer = SentenceTransformer("paraphrase-multilingual-mpnet-base-v2",
                                                                    device="cpu")
        cls._resources_dir: str = f"{Path(__file__).parent.parent}/_resources"
        cls._test_dir: str = f"{gettempdir()}/{cls.__name__}"
        cls._test_download_dir: str = os.environ.get("TEST_DOWNLOADS_HOME",
                                                     f"{gettempdir()}/test-downloads/{cls.__name__}")
        makedirs(cls._test_download_dir, exist_ok=True)

    def setUp(self):
        self._intent_store = IntentStore()
        nlp = spacy.load("de_core_news_sm")
        self._class_under_test: IntentDetection = IntentDetection(nlp,
                                                                  self._transformer,
                                                                  self._intent_store,
                                                                  )

    def tearDown(self):
        rmtree(self._test_dir, ignore_errors=True)
        super().tearDown()

    def test_preprocessing_works_for_supported_language(self):
        result = self._class_under_test._preprocess("de", "das ist ein intent der preprocessed worden ist")
        self.assertEqual("Das ist ein Intent der Preprocessed worden ist", result)

    def test_preprocessing_returns_same_text_for_unsupported_language(self):
        result = self._class_under_test._preprocess("en", "das ist ein intent der preprocessed worden ist")
        self.assertEqual("das ist ein intent der preprocessed worden ist", result)

    def test_preprocessing_replaces_synonyms(self):
        result = self._class_under_test._preprocess("de", "ÖH-Beitrag")
        self.assertEqual("Uni steuer", result)

    def test_detect(self):
        # given
        intents_data = Path(f"{self._resources_dir}/00000000-0000-0000-0000-000000000000-intents.json").read_bytes()
        self._intent_store.add_intents(json.loads(intents_data.decode("utf-8")))
        embeddings_data = Path(
            f"{self._resources_dir}/00000000-0000-0000-0000-000000000000-embeddings.pkl").read_bytes()
        self._intent_store.add_embeddings("00000000-0000-0000-0000-000000000000", pickle.loads(embeddings_data))
        request = IntentDetectionRequest(
            "ich bin hunger",
            ["00000000-0000-0000-0000-000000000000"],
            ["de"],
            0.8,
            1
        )
        # when
        result = self._class_under_test.detect(request)
        # then
        self.assertEqual(len(result.matches), 1)
        self.assertEqual(result.matches[0].identityId, "00000000-0000-0000-0000-000000000000")
        self.assertEqual(result.matches[0].intentId, "22222222-2222-2222-2222-222222222222")
        self.assertGreater(result.matches[0].score, 0.8)
        self.assertEqual(result.matches[0].language, "de")
        self.assertEqual(result.matches[0].utterance, "Ich habe hunger")

    def test_detect_ignores_language(self):
        # given
        intents_data = Path(f"{self._resources_dir}/00000000-0000-0000-0000-000000000000-intents.json").read_bytes()
        self._intent_store.add_intents(json.loads(intents_data.decode("utf-8")))
        embeddings_data = Path(
            f"{self._resources_dir}/00000000-0000-0000-0000-000000000000-embeddings.pkl").read_bytes()
        self._intent_store.add_embeddings("00000000-0000-0000-0000-000000000000", pickle.loads(embeddings_data))
        request = IntentDetectionRequest(
            "wer bist du",
            ["00000000-0000-0000-0000-000000000000"],
            ["de"],
            0.8,
            3
        )

        # when
        result = self._class_under_test.detect(request)
        # then
        self.assertEqual(len(result.matches), 2)
        self.assertEqual(result.matches[0].utterance, "Wer bist du")
        self.assertEqual(result.matches[1].utterance, "Wer Bischn du")

    def test_detect_ignores_missing_musts(self):
        # given
        intents_data = Path(f"{self._resources_dir}/00000000-0000-0000-0000-000000000000-intents.json").read_bytes()
        self._intent_store.add_intents(json.loads(intents_data.decode("utf-8")))
        embeddings_data = Path(
            f"{self._resources_dir}/00000000-0000-0000-0000-000000000000-embeddings.pkl").read_bytes()
        self._intent_store.add_embeddings("00000000-0000-0000-0000-000000000000", pickle.loads(embeddings_data))
        request = IntentDetectionRequest(
            "Ich habe Schwein",
            ["00000000-0000-0000-0000-000000000000"],
            ["de"],
            0.7,
            3
        )

        # when
        with self.assertLogs() as capture:
            result = self._class_under_test.detect(request)
            # then
            self.assertEqual(1, len(result.matches))
            self.assertEqual(result.matches[0].utterance, "Ich war grantig")
            self.assertIn(
                "INFO:src.intent_detection.intent_detection:Ignoring utterance 'Ich habe hunger' because of missing "
                "must words [Must(text='hunger', fuzzy=True)]",
                capture.output)

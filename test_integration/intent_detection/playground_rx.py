import time
from typing import List

import rx
from rx import operators
from rx.operators import observe_on, do_action, take, buffer_with_count
from rx.scheduler import ThreadPoolScheduler


# rx.from_iterable([{"foo": x} for x in range(101)]).pipe(
#     observe_on(ThreadPoolScheduler(1)),
#     operators.map(lambda x: {"bar": x})
# ).subscribe(
#     on_next=lambda x: print(f"Got {x}"),
#     on_error=lambda e: print(f"Error: {str(e)}"),
#     on_completed=lambda: print("Done"),
# )
# time.sleep(10)

def foo(foo: dict, bar: dict) -> dict:
    foo.get("items").append(bar)
    return foo


rx.from_iterable([{"foo": x} for x in range(101)]).pipe(
    buffer_with_count(10),
    operators.flat_map(lambda batch: rx.from_iterable(batch).pipe(
        operators.reduce(foo, {"items": []})
    )),
    observe_on(ThreadPoolScheduler(1)),
    operators.map_indexed(lambda x, i: {"no": i, "bar": x}),
    operators.do_action(lambda x: print(f"Got: {x}"))
).run()

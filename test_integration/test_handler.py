import os
from os import makedirs
from os.path import basename
from pathlib import Path
from shutil import rmtree, copyfile
from tempfile import gettempdir
from typing import List
from unittest import TestCase
from unittest.mock import Mock
from urllib.request import urlretrieve

from gaia_sdk.api.data.DataRef import DataRef
from gaia_sdk.gaia import GaiaRef
from gaia_sdk.http.response.FileListing import FileListing
from rx import just
from rx.core.typing import Observable

from src import handler
from src.handler import Property
from src.intent_detection.intent_store import IntentStore


class HandlerIntegrationTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls._resources_dir: str = f"{Path(__file__).parent}/_resources"
        cls._test_dir: str = f"{gettempdir()}/{cls.__name__}"
        cls._test_download_dir: str = os.environ.get("TEST_DOWNLOADS_HOME",
                                                     f"{gettempdir()}/test-downloads/{cls.__name__}")
        makedirs(cls._test_download_dir, exist_ok=True)

    def tearDown(self):
        rmtree(self._test_dir, ignore_errors=True)
        super().tearDown()

    def test_on_started(self):
        # TODO: Adjust for synonyms
        # given
        test_dir = f"{self._test_dir}/{self.test_on_started.__name__}"
        makedirs(test_dir, exist_ok=True)
        context = {"dependencies": {"data": [
            "https://github.com/explosion/spacy-models/releases/download/de_core_news_sm-2.3.0/de_core_news_sm-2.3.0.tar.gz",
            "https://sbert.net/models/paraphrase-multilingual-mpnet-base-v2.zip"
        ]}}
        os.environ[Property.INTENTS_LOCATION.name] = \
            "gaia://f000f000-f000-f000-f000-f000f000f000/intent-detection/transformer"
        os.environ[Property.TRANSFORMER_LOCATION.name] = \
            "gaia://f000f000-f000-f000-f000-f000f000f000/intent-detection/transformer/paraphrase-multilingual-mpnet-base-v2_lev5_frozen_grads.pt"
        intent_store = IntentStore()
        self._download_file(context["dependencies"]["data"][0], test_dir)
        self._download_file(context["dependencies"]["data"][1], test_dir)


        def __data_list_fun_mock() -> Observable[List[FileListing]]:
            return just([
                FileListing({"tenant": "fd827c9c-3d81-4dcf-8baf-16cb2cfe223c", "filePath":
                    "foo.bar"}),  # some irrelevant file
                FileListing({"tenant": "fd827c9c-3d81-4dcf-8baf-16cb2cfe223c", "filePath":
                    "intent-detection/transformer/00000000-0000-0000-0000-000000000000-meta.json"}),
                FileListing({"tenant": "fd827c9c-3d81-4dcf-8baf-16cb2cfe223c", "filePath":
                    "intent-detection/transformer/00000000-0000-0000-0000-000000000000-intents.json"}),
                FileListing({"tenant": "fd827c9c-3d81-4dcf-8baf-16cb2cfe223c", "filePath":
                    "intent-detection/transformer/00000000-0000-0000-0000-000000000000-embeddings.pkl"}),
            ])

        __data_list_mock = Mock()
        __data_list_mock.list = __data_list_fun_mock
        __data_intents_bytes_mock = Mock()
        __data_intents_bytes_mock.as_bytes = Mock(
            return_value=just(Path(
                f"{self._resources_dir}/00000000-0000-0000-0000-000000000000-intents.json").read_bytes()))
        __data_embeddings_bytes_mock = Mock()
        __data_embeddings_bytes_mock.as_bytes = Mock(
            return_value=just(Path(
                f"{self._resources_dir}/00000000-0000-0000-0000-000000000000-embeddings.pkl").read_bytes()))
        __data_model_bytes_mock = Mock()
        __data_model_bytes_mock.as_bytes = Mock(
            return_value=just(Path(
                f"{self._resources_dir}/paraphrase-multilingual-mpnet-base-v2_lev5_frozen_grads.pt.pt").read_bytes()))

        def __data_fun_mock(uri: str) -> DataRef:
            if uri == "gaia://f000f000-f000-f000-f000-f000f000f000/intent-detection/transformer":
                return __data_list_mock
            elif uri == "gaia://f000f000-f000-f000-f000-f000f000f000/intent-detection/transformer/00000000-0000-0000-0000-000000000000-intents.json":
                return __data_intents_bytes_mock
            elif uri == "gaia://f000f000-f000-f000-f000-f000f000f000/intent-detection/transformer/00000000-0000-0000-0000-000000000000-embeddings.pkl":
                return __data_embeddings_bytes_mock
            elif uri == "gaia://f000f000-f000-f000-f000-f000f000f000/intent-detection/transformer/paraphrase-multilingual-mpnet-base-v2_lev5_frozen_grads.pt":
                return __data_model_bytes_mock
            raise Exception(f"No handling implemented for {uri}")

        sdk_mock: GaiaRef = Mock()
        sdk_mock.data = __data_fun_mock

        # for manual testing
        # sdk_mock = Gaia.connect("http://api.aios.local", HMACCredentials("default", "default"))

        # when
        with self.assertLogs() as capture:
            handler._start(context, test_dir, test_dir, intent_store, sdk_mock)
            # then
            intents = intent_store.get_intents("00000000-0000-0000-0000-000000000000")
            self.assertEqual(8, len(intents.utterances))
            self.assertIsNotNone(intents.embeddings)
            self.assertIn("INFO:src.handler:Preparing models", capture.output)
            self.assertIn("INFO:src.handler:Initializing", capture.output)
            self.assertTrue([x for x in capture.output if x.startswith("INFO:src.handler:Initialized trained model in ")],
                            f"{capture.output} does not contain 'INFO:src.handler:Initialized trained model in '")

    def _download_file(self, url, test_dir):
        file_dest_path = f"{self._test_download_dir}/{basename(url)}"
        if not Path(file_dest_path).exists():
            urlretrieve(url, file_dest_path)
        copyfile(file_dest_path, f"{test_dir}/{basename(url)}")
